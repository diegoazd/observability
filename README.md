Observability template
-----------------------

Archivos relevantes: 

    <observability>
      |
      +- src
         |
         +- rest
             |
             +- build.gradle
             |
             +- Dockerfile
             |
             +- src
                 |
                 +- main
                     |
                     +- groovy
                         |
                         +- mx.com.samples.rest
                                             |
                                             +- RestApplication.groovy
                                             |
                                             +- services.ReviewsServices.groovy
                                            
### build.gradle
  
```  
  compile "io.opentracing.contrib:opentracing-spring-cloud-starter:{version}"  
  compile "io.jaegertracing:jaeger-tracerresolver:{version}"  
```  
  
### Dockerfile  
___  
  
```
  FROM fabric8/java-jboss-openjdk8-jdk:1.3.1
  LABEL maintainer="diegoazd@gmail.com"
  ENV TZ=America/Mexico_City
  ENV JAVA_APP_JAR rest-0.1.0-SNAPSHOT.jar
  ENV AB_OFF true
  ENV JAEGER_SERVICE_NAME=reviews-api\
    JAEGER_ENDPOINT=http://jaeger-collector.istio-system.svc:14268/api/traces\
    JAEGER_PROPAGATION=b3\
    JAEGER_SAMPLER_TYPE=const\
    JAEGER_SAMPLER_PARAM=1
  ADD build/libs/rest-0.1.0-SNAPSHOT.jar /deployments/
  EXPOSE 8080
```
  
### RestApplication.groovy  
  
  
```
  @Bean
  io.opentracing.Tracer tracer() {
    if(System.getenv(Configuration.JAEGER_SERVICE_NAME) == null) {
      return new Configuration(applicationName).getTracer()
    }

    Configuration.fromEnv().getTracer()
  }
```  

### ReviewsServices.groovy
```
      final String moviesHost = System.getenv().getOrDefault("MOVIES_API_SERVICE_HOST", "localhost")    
```  
  
### Correr localmente el proyecto . 

1. Correr una instancia de jaeger localmente.  
 ```docker run -d --name jaeger \                                                                                                  
    -e COLLECTOR_ZIPKIN_HTTP_PORT=9411 -e QUERY_BASE_PATH=/jaeger \  
    -p 5775:5775/udp \  
    -p 6831:6831/udp \  
    -p 6832:6832/udp \  
    -p 5778:5778 \  
    -p 16686:16686 \  
    -p 14268:14268 \  
    -p 9411:9411 \  
    jaegertracing/all-in-one:1.6```
 
  
2. Correr proyecto rest   
    ./gradlew :rest:bootRun  
  
3. Correr proyecto movies  
    ./gradlew :movies:bootRun
  
4. Hacer una peticion a:  
    curl http://localhost:8080/reviews
    
5. Ir a jaeger localmente localhost:16686

### Construir las imagenes de docker  

1. Reinicializar gcloud
    `gcloud init(seleccionar proyecto kubo-devops-management, seleccionar zona por default us-central1-a)`
2. Configurar registry de gcloud  
    `gcloud auth configure-docker`  
3. En la carpeta raiz del proyecto:  
    `./gradlew :rest:build`  
    `./gradlew :movies:build`  
4. Construir las imagenes de cada proyecto   
    Dentro de la carpeta movies ejecutar:  
    `docker build -t gcr.io/kubo-devops-management/{tagName}:{tagVersion} .`
    `docker push {dockerTag}:{dockerVersion}`  
    Dentro de la carpeta rest ejecutar:  
    `docker build -t gcr.io/kubo-devops-management/{tagName}:{tagVersion} .`  
    `docker push {dockerTag}:{dockerVersion}`  
