package mx.com.samples.movies.rest

import mx.com.samples.movies.services.MoviesServices
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

import java.util.concurrent.TimeUnit

@RestController
@RequestMapping("/movies")
class MoviesController {

  @Autowired
  MoviesServices moviesServices

  @GetMapping("/top")
  Map getTopMovies() {
    moviesServices.topMovies()
  }
}
