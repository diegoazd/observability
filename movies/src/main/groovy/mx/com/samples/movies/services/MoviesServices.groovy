package mx.com.samples.movies.services

import org.springframework.stereotype.Service
import java.util.concurrent.TimeUnit

@Service
class MoviesServices {

  Map topMovies() {
    Integer sleepTime = Math.random() * 700
    TimeUnit.MILLISECONDS.sleep(50 + sleepTime)

    if(sleepTime > 400)
      throw RuntimeException("Error finding music")


    [
        Roma: [
            [rating: 100, reviewer: 'Eric Kohn', review: 'Roma is by far the most experimental storytelling in a career filled with audacious (and frequently excessive) gimmicks.'],
            [rating: 100, reviewer: 'Robbie Collon', review: 'Every individual scene feels filled with the lucid detail of a formative recollection or a recurring dream.'],
            [rating: 100, reviewer: 'Todd McCarthy', review: 'Roma may not be the memoir film many might have expected from such an adventurous ...'],
            [rating: 100, reviewer: 'Peter Bradshaw', review: 'At times it feels novelistic, a densely realised, intimate drama giving us access to domestic lives developing in what feels like real time'],
            [rating: 100, reviewer: 'Stephanie Zacharek', review: 'This glorious, tender picture, a memoir written in film language, is only indirectly about the man who made it.']
        ],
    ]
  }
}
