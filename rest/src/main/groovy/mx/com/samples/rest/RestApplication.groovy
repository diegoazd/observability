package mx.com.samples.rest

import io.jaegertracing.Configuration
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.context.annotation.Bean
import org.springframework.web.client.RestTemplate

@SpringBootApplication
class RestApplication {

  @Value('${spring.application.name}')
  private String applicationName

  @Bean
  io.opentracing.Tracer tracer() {
    if(System.getenv(Configuration.JAEGER_SERVICE_NAME) == null) {
      return new Configuration(applicationName).getTracer()
    }

    Configuration.fromEnv().getTracer()
  }

  @Bean
  RestTemplate restTemplate(RestTemplateBuilder restTemplateBuilder) {
    restTemplateBuilder.build()
  }


  static void main(String[] args) {
    SpringApplication.run(RestApplication, args)
  }
}
