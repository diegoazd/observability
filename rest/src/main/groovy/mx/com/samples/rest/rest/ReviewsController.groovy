package mx.com.samples.rest.rest

import mx.com.samples.rest.services.ReviewsServices
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/reviews")
class ReviewsController {

  @Autowired
  ReviewsServices moviesServices

  @GetMapping
  Map getMoviesReviews() {
    moviesServices.getReviews()
  }
}
