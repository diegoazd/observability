package mx.com.samples.rest.services

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import org.springframework.web.client.RestTemplate

@Service
class ReviewsServices {

  final String moviesHost = System.getenv().getOrDefault("MOVIES_API_SERVICE_HOST", "localhost")
  final String moviesPort = System.getenv().getOrDefault("MOVIES_API_SERVICE_PORT", "8081")

  @Autowired
  private RestTemplate restTemplate

  @Autowired
  private io.opentracing.Tracer tracer

  Map getReviews() {
    [movies: getMoviesReviews()]
  }

  private Map getMoviesReviews() {
    tracer.activeSpan()?.setBaggageItem("transaction", "reviews")

    String resourceUrl = "http://${moviesHost}:${moviesPort}/movies/top"
    ResponseEntity response = restTemplate.getForEntity(resourceUrl, Map.class)

    response.body
  }
}
